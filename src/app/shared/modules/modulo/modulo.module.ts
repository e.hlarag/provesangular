import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Exercici1ComponentComponent } from '../../../view/exercici1-component/exercici1-component.component';
import {ReactiveFormsModule} from "@angular/forms";
import { AreaComponent } from '../../../view/area/area.component';
import { VocalsComponent } from '../../../view/vocals/vocals.component';
import { Exercici3PaquetComponent } from '../../../view/exercici3-paquet/exercici3-paquet.component';



@NgModule({
  declarations: [
    Exercici1ComponentComponent,
    AreaComponent,
    VocalsComponent,
    Exercici3PaquetComponent
  ],
    imports: [
        CommonModule,
        ReactiveFormsModule
    ],
  exports: [
    Exercici1ComponentComponent,
    AreaComponent,
    VocalsComponent,
    Exercici3PaquetComponent
  ]
})
export class ModuloModule { }
