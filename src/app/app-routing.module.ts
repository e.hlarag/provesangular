import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {Exercici1ComponentComponent} from "./view/exercici1-component/exercici1-component.component";
import {AreaComponent} from "./view/area/area.component";
import {VocalsComponent} from "./view/vocals/vocals.component";
import {Exercici3PaquetComponent} from "./view/exercici3-paquet/exercici3-paquet.component";

const routes: Routes = [
  {path:'Exercici1', component:Exercici1ComponentComponent},
  {path:'Exercici2', component:AreaComponent},
  {path:'Exercici3', component:VocalsComponent},
  {path:'Exercici4',component:Exercici3PaquetComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
