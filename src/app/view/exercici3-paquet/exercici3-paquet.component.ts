import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from "@angular/forms";

// @ts-ignore
import Suma from 'hectorlarapackage'


@Component({
  selector: 'app-exercici3-paquet',
  templateUrl: './exercici3-paquet.component.html',
  styleUrl: './exercici3-paquet.component.css'
})
export class Exercici3PaquetComponent implements OnInit{
form!:FormGroup;
numerofinal!:number;
resultat!:string;
ngOnInit(){
  this.form=new FormGroup({
    num1:new FormControl(0,[Validators.required]),
    num2:new FormControl(0,[Validators.required])
  })
}

Calcular(){
  this.numerofinal=Suma(this.form.controls['num1'].value,this.form.controls['num2'].value);
  this.resultat="El resultat és: "+this.numerofinal;
}
}
