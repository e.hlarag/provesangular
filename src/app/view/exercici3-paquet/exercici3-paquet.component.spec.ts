import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Exercici3PaquetComponent } from './exercici3-paquet.component';
import {ReactiveFormsModule} from "@angular/forms";


describe('Exercici3PaquetComponent', () => {
  let component: Exercici3PaquetComponent;
  let fixture: ComponentFixture<Exercici3PaquetComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [Exercici3PaquetComponent],
      imports:[ReactiveFormsModule]
    })
    .compileComponents();

    fixture = TestBed.createComponent(Exercici3PaquetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it ('Prova comprova num1',()=>{
    component.form.controls['num1'].setValue(5);
    fixture.detectChanges();
    expect(component.form.controls['num1'].value).toBe(5)
  });

  it ('Prova comprova num2',()=>{
    component.form.controls['num2'].setValue(10);
    fixture.detectChanges();
    expect(component.form.controls['num2'].value).toBe(10)
  });

  it ('Prova comprova funció',()=>{
    component.form.controls['num1'].setValue(3);
    component.form.controls['num2'].setValue(2);
    component.Calcular();
    fixture.detectChanges();
    expect(component.numerofinal).toBe(5)
  });

  it ('Prova resultat html',()=>{
    component.form.controls['num1'].setValue(3);
    component.form.controls['num2'].setValue(3);
    component.Calcular();
    fixture.detectChanges();
    const compiled=fixture.debugElement.nativeElement;
    expect(compiled.querySelector('p').textContent).toBe("El resultat és: 6")
  });
});
