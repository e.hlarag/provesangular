import { ComponentFixture, TestBed } from '@angular/core/testing';

import { VocalsComponent } from './vocals.component';
import {ReactiveFormsModule} from "@angular/forms";

describe('VocalsComponent', () => {
  let component: VocalsComponent;
  let fixture: ComponentFixture<VocalsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [VocalsComponent],
      imports:[ReactiveFormsModule]
    })
    .compileComponents();

    fixture = TestBed.createComponent(VocalsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it ('Prova comprova input',()=>{
    component.vocals.setValue('a');
    fixture.detectChanges();
    expect(component.vocals.value).toBe('a');
  })

  it ('Prova comprova resultat',()=>{
    component.vocals.setValue('aeiou');
    component.contarVocales();
    fixture.detectChanges();
    const compiled=fixture.debugElement.nativeElement;
    expect(compiled.querySelector('p').textContent).toBe('a: 1, e: 1, i: 1, o: 1, u: 1');
  })

});
