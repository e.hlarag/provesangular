import { Component } from '@angular/core';
import {FormControl, Validators} from "@angular/forms";

@Component({
  selector: 'app-vocals',
  templateUrl: './vocals.component.html',
  styleUrl: './vocals.component.css'
})
export class VocalsComponent {
vocals:FormControl= new FormControl('',[Validators.required]);
resultat!:string;
a:number = 0;
e:number = 0;
i:number= 0;
o:number = 0;
u:number = 0;
  contarVocales() {
    this.o=0;
    this.u=0;
    this.i=0;
    this.a=0;
    this.e=0;
    let valor = this.vocals.value;
    let lengthval = valor.length;
    for (let i = 0; i < lengthval; i++) {
      let letra = valor.charAt(i);
      if (letra == 'a' || letra == 'A') {
        this.a++;
      }
      if (letra == 'e' || letra == 'E') {
        this.e++;
      }
      if (letra == 'i' || letra == 'I') {
        this.i++;
      }
      if (letra == 'o' || letra == 'O') {
        this.o++;
      }
      if (letra == 'u' || letra == 'U') {
        this.u++;
      }
    }
    this.resultat = "a: " + this.a + ", e: " + this.e + ", i: " + this.i + ", o: " + this.o + ", u: " + this.u;
    console.log(this.vocals.value)
  }
}
