import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Exercici1ComponentComponent } from './exercici1-component.component';
import {ReactiveFormsModule} from "@angular/forms";

describe('Exercici1ComponentComponent', () => {
  let component: Exercici1ComponentComponent;
  let fixture: ComponentFixture<Exercici1ComponentComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [Exercici1ComponentComponent],
      imports:[ReactiveFormsModule]
    })
    .compileComponents();

    fixture = TestBed.createComponent(Exercici1ComponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it ('Prova Comprova número',()=>{
    component.myform.controls['num1'].setValue(35);
    fixture.detectChanges();
    expect(component.myform.controls['num1'].value).toBe(35);
  })

  it ('Prova Comprova funció',()=>{
    component.myform.controls['num1'].setValue(35);
    component.myform.controls['num2'].setValue(22);
    component.Calcular();
    fixture.detectChanges();
    expect(component.dada).toBe(57)
  })

  it ('Prova Element del DOM',()=>{
    component.myform.controls['num1'].setValue(35);
    component.myform.controls['num2'].setValue(22);
    component.Calcular();
    fixture.detectChanges();
    const compiled=fixture.debugElement.nativeElement;
    expect(compiled.querySelector('span').textContent).toBe("El resultat és: 57");
  })


});
