import { Component } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";

@Component({
  selector: 'app-exercici1-component',
  templateUrl: './exercici1-component.component.html',
  styleUrl: './exercici1-component.component.css'
})
export class Exercici1ComponentComponent {
  dada!:number;

  constructor(private formBuilder:FormBuilder) {
  }
  myform:FormGroup=this.formBuilder.group({
    num1:[0,[Validators.min(1), Validators.max(1000), Validators.required]],
    num2:[0,[Validators.min(1), Validators.max(1000), Validators.required]]
  })

  Calcular():void{
    this.dada=this.myform.controls['num1'].value+this.myform.controls['num2'].value;
  }

}
