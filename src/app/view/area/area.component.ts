import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from "@angular/forms";

@Component({
  selector: 'app-area',
  templateUrl: './area.component.html',
  styleUrl: './area.component.css'
})
export class AreaComponent implements OnInit{
  form!:FormGroup;
  resultat!:string;
  area!:number;
  ngOnInit(){
    this.form= new FormGroup({
      base:new FormControl(0,[ Validators.required, Validators.min(1), Validators.max(1000)]),
      altura:new FormControl(0, [Validators.required, Validators.min(1), Validators.max(1000)])
    })
  }
  Calcular(){
    this.area=(this.form.controls['base'].value*this.form.controls['altura'].value)/2;
    this.resultat="L'àrea és: "+this.area;
  }

}
