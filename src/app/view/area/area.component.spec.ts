import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AreaComponent } from './area.component';
import {ReactiveFormsModule} from "@angular/forms";

describe('AreaComponent', () => {
  let component: AreaComponent;
  let fixture: ComponentFixture<AreaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [AreaComponent],
      imports:[ReactiveFormsModule]
    })
    .compileComponents();

    fixture = TestBed.createComponent(AreaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it ('Prova comprova base',()=>{
    component.form.controls['base'].setValue(5);
    fixture.detectChanges();
    expect(component.form.controls['base'].value).toBe(5)
  })
  it ('Prova comprova alçada',()=>{
    component.form.controls['altura'].setValue(10);
    fixture.detectChanges();
    expect(component.form.controls['altura'].value).toBe(10)
  })

  it ('Prova comprova funció',()=>{
    component.form.controls['base'].setValue(3);
    component.form.controls['altura'].setValue(2);
    component.Calcular();
    fixture.detectChanges()
    expect(component.area).toBe(3)
  })

  it ('Prova resultat html',()=>{
    component.form.controls['base'].setValue(3);
    component.form.controls['altura'].setValue(3);
    component.Calcular();
    fixture.detectChanges()
    const compiled=fixture.debugElement.nativeElement;
    expect(compiled.querySelector('p').textContent).toBe("L'àrea és: 4.5")
  })

});
